# frozen_string_literal: true

require_relative 'lib/sourcewarp/version'

Gem::Specification.new do |spec|
  spec.name          = 'sourcewarp'
  spec.version       = SourceWarp::VERSION
  spec.authors       = ['Julian Thome', 'James Johnson']
  spec.email         = ['jthome@gitlab.com', 'jamxjohn@gmail.com']

  spec.summary       = 'SourceWarp - A tool to collect metrics while replay events from a GitLab project'
  spec.description   = 'SourceWarp - A tool to collect metrics while replay events from a GitLab project'
  spec.homepage      = 'https://gitlab.com/gitlab-org/vulnerability-research/foss/sourcewarp'
  spec.license       = ''
  spec.required_ruby_version = Gem::Requirement.new('>= 2.6.0')

  spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/gitlab-org/vulnerability-research/foss/sourcewarp'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/gitlab-org/vulnerability-research/foss/sourcewarp/CHANGELOG'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.glob('{exe,bin,lib}/**/*') + %w[README.md LICENSE.txt]
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'cgi'
  spec.add_dependency 'csv'
  spec.add_dependency 'fileutils'
  spec.add_dependency 'git'
  spec.add_dependency 'gitlab', '~> 4.17'
  spec.add_dependency 'tmpdir'

  spec.add_development_dependency 'pry', '~> 0.13.1'
  spec.add_development_dependency 'pry-byebug', '~> 3.9'
  spec.add_development_dependency 'rspec', '~>3.10'
  spec.add_development_dependency 'rspec-mocks', '~>3.10'
  spec.add_development_dependency 'rspec-parameterized', '~> 0.4.2'
  spec.add_development_dependency 'rubocop', '~> 1.8'
  spec.add_development_dependency 'simplecov', '~> 0.21.2'
  spec.add_development_dependency 'solargraph'
end
