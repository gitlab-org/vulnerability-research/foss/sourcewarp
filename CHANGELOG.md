## v1.1.0
- Branch names are configurable now (!3)
- Example config (!3)
- Docker and Git functions modified (!3)

## v1.0.0
- Initial import
