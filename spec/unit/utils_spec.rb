require 'rspec'
require_relative '../../lib/sourcewarp/utils'

RSpec.describe SourceWarp::Utils do
  context 'set default' do
    it 'should work correctly' do
      expect(SourceWarp::Utils.default_env("K", "default")).to eql("default")
    end
  end
  context 'slash_to_unserscore' do
    it 'should work correctly' do
      expect(SourceWarp::Utils.slash_to_underscore("this/is/a/dir")).to eql("this_is_a_dir")
    end
  end
  context 'diff files' do
    it 'should be post processed correctly' do
      rewritten_diff = SourceWarp::Utils.rewrite_diff('spec/testdata/mr0.patch', ['README.md'])
      expect(File.read('spec/testdata/expect0.patch')).to eql(rewritten_diff)
      rewritten_diff = SourceWarp::Utils.rewrite_diff('spec/testdata/mr0.patch', ['pom.xml'])
      expect(File.read('spec/testdata/expect1.patch')).to eql(rewritten_diff)
      rewritten_diff = SourceWarp::Utils.rewrite_diff('spec/testdata/mr0.patch', ['pom.xml', '.*\\.xml'])
      expect(File.read('spec/testdata/expect1.patch')).to eql(rewritten_diff)
      rewritten_diff = SourceWarp::Utils.rewrite_diff('spec/testdata/mr0.patch', ['pom.xml', '.*\\.md'])
      expect("").to eql(rewritten_diff)
      rewritten_diff = SourceWarp::Utils.rewrite_diff('spec/testdata/mr0.patch', [], ['.*\\.xml'])
      expect(File.read('spec/testdata/expect0.patch')).to eql(rewritten_diff)
      rewritten_diff = SourceWarp::Utils.rewrite_diff('spec/testdata/mr0.patch', [], ['.*\\.md'])
      expect(File.read('spec/testdata/expect1.patch')).to eql(rewritten_diff)
    end
  end
end
