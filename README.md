# SourceWarp

SourceWarp is a record-and-replay tool for benchmarking/testing. A short demo is available on [GitLab Unfiltered](https://www.youtube.com/watch?v=-9lk_Jhuq14). In case you use this tool as part of a research project, please use the bibtex reference below:

``` bash
@INPROCEEDINGS{sourcewarp,
  author={Thome, Julian and Johnson, James and Dawson, Isaac and Bolkensteyn, Dinesh and Henriksen, Michael and Art, Mark} 
  booktitle={2023 IEEE/ACM International Conference on Automation of Software Test (AST)}, 
  title={SourceWarp: A scalable, SCM-driven testing and benchmarking approach to support data-driven and agile decision making for CI/CD tools and DevOps platforms}, 
  year={2023}
}
```

## Tool Authors 

This tool has been mainly authored by Julian Thome `<jthome@gitlab.com>` and
James Johnson `<jamxjohn@gmail.com>` and is currently maintained by the
[Vulnerability Research Team](https://about.gitlab.com/handbook/engineering/development/sec/secure/vulnerability-research/).

## Overview

SourceWarp replays events of a source project to a System Under Test (SUT)
which could be GitLab instances and/or Docker containers. User-provided metric
collection scripts are run to collect data from the target GitLab instance when
the events below occur:

* Initial project setup (prior to replaying any events)
* After merging a merge request

Additional event types could be supported in the future (issues, epics, etc.).
The examples below illustrate how SourceWarp could be applied in the context of
GitLab.

```mermaid
graph LR
    subgraph G_s["GitLab Source Instance"]
        subgraph P_s["Project Source"]
            merge_request_s1["Merge Request 1"]
            merge_request_s2["Merge Request 2"]
            merge_request_s3["Merge Request 3"]
        end
    end

    subgraph G_t["GitLab Target Instance"]
        subgraph P_t["Project Target"]
            merge_request_t1["Merge Request 1"]
            merge_request_t2["Merge Request 2"]
            merge_request_t3["Merge Request 3"]
        end
    end

    subgraph Metrics1
        metrics1["AAAA"]
        metrics2["BBBB"]
        metrics3["CCCC"]
    end

    merge_request_s1 -->|open| merge_request_t1
    merge_request_s2 -->|open| merge_request_t2
    merge_request_s3 -->|open| merge_request_t3

    merge_request_t1 -->|merge| merge_request_t1
    merge_request_t2 -->|merge| merge_request_t2
    merge_request_t3 -->|merge| merge_request_t3

    merge_request_t1 -->|metrics| metrics1
    merge_request_t2 -->|metrics| metrics2
    merge_request_t3 -->|metrics| metrics3
```

Replaying a project twice (once each onto two different versions of GitLab)
will yield two sets of metrics that can be compared and analyzed. Ideally
this leads to insights about the impact of the code changes between the two
GitLab instances:

```mermaid
graph LR
    subgraph Metrics1
        metrics11["AAAA"]
        metrics12["BBBB"]
        metrics13["CCCC"]
    end

    subgraph Metrics2
        metrics21["AAAA"]
        metrics22["ZZZZ"]
        metrics23["CCCC"]
    end

    metrics11 --> metrics21
    metrics12 -->|"different"| metrics22
    metrics13 --> metrics23

    classDef green fill:#9f9,stroke-width:4px,font-weight:bold;
    classDef red fill:#f99,stroke-width:4px,font-weight:bold;
  
    class metrics12 green
    class metrics22 green
```

## Using SourceWarp

SourceWarp can be divided into two phases: (1) a _Record_ phase that records MRs from
a _source_ project and (2) a _Replay_ phase where the MR sequence is replayed
on a _target_ project.

The source and target projects can be configured through the `SRC_PROJECT`
and `DST_PROJECT` variables, respectively.

``` bash
# source project
export SRC_GITLAB_TOKEN="..."
export SRC_PROJECT="testreplay-source"
# target project
export DST_GITLAB_TOKEN="..."
export DST_PROJECT="testreplay-target"
```

### Record Phase

In the record phase, merge request commits are extracted from the git history
of the source project. It is possible to filter merge request commits based on
the time in which they have been merged through the `--until` and `--since`
parameters. You can specify how long SourceWarp looks back into the past by configuring
the `LOG_HISTORY_SIZE` variable which defaults to 1000000 commit log entries.

By means of the extracted merge request commits of the source project, SourceWarp then
synthesizes a sequence of patches by means of the `merge_commit_sha` property
that captures the SHA of the main branch after the MR has been merged in. In
essence, SourceWarp computes the diffs between all the `merge_commit_sha` properties
of the MRs to make sure that commits that were not channelled through MRs are
also reflected.  All the collected information is persisted in the directory
defined through the `RECORD_STORE` variable which defaults to
`/tmp/record`.

The record directory has the structure depicted below where `<source project>`
is a placeholder for name of the source project defined through
`SRC_PROJECT`.  The `diffs` directory contains the sequence of diffs that
were synthesized from the MRs of the source projects. The `src_project` is the
actual git repository of the source project and `src_project_init_files`
contains the start state of the source project, i.e., the initial state for the
replay to begin with. The initial state is determined by the first relevant
`merge_commit_sha` from the extracted MR sequence.

``` bash
record
└──<source project>
    ├── diffs
    ├── src_project (git repo)
    └── src_project_init_files (extracted files for starting state)
```

You can let SourceWarp ignore files that should not be tracked throughout the replay.
This is useful to focus an SourceWarp test run on only relevant files and avoids
issues caused by changes on the GitLab CI configuration. You can add the
comma-separated regular expression patterns that match files to be ingored, or
considered to the `IRRELEVANT` and/or `RELEVANT` variables,
respectively. SourceWarp will then ignore all the files that match the defined
pattern(s). In case you have a list of files you would like to be tracked, you
can add them to a file (one line per file to track) and set `RELEVANT_FILE`
to the path of the file. This is useful if you would like SourceWarp to focus on MRs
that focus only on changes that have been applied to certain files.

There is a slight difference in the way `RELEVANT`, `RELEVANT_FILE`,
and `IRRELEVANT` take effect: as opposed to `IRRELEVANT`,
`RELEVANT` and `RELEVANT_FILE` do not have an effect on the file
selection when importing the initial state of the source project. 

As a sensible default, it makes sense to ignore the `.gitlab-ci.yml` files.

``` bash
export IRRELEVANT=".gitlab-ci.yml"
```

### Replay Phase

The replay phase leverages record store. SourceWarp creates an MR for every diff that
was synthesized in the record phase. As depicted below,  SourceWarp clones the target
project into the record store, where `<target project>` is a placeholder for
the name of the target project configured through `DST_PROJECT`,  and
`dst_project` is the Git repository. SourceWarp then imports the initial state from
the source project by copying over the files from `src_project_init_files` and
pushes this state onto the main branch.

``` bash
record
    ├──<target project> 
    │   └── dst_project (git repo)
```

After the initial import, SourceWarp applies the patches in the `diffs` directory one
by one and executes the following loop:

```mermaid
graph TD
A[Start] --> Import[Import Initial State]
Import --> Overrides[Apply Overrides]
Overrides --> C{Patch available?}
C -->|No| Write[Write metrics]
Write --> End
C -->|Yes| Apply[Apply Patch on dst_project]
Apply --> Stats1[Collect Metrics before] 
Stats1 --> Commit[Commit and Push to branch]
Commit --> MR[Create MR and auto-accept]
MR --> Merged{Changes meged to main branch?}
Merged -->|Yes| Pull[Pull changes and checkout main branch]
Pull --> Stats2[Collect Metrics after] 
Stats2 --> C
Merged -->|No| WAIT[Wait x seconds]
WAIT --> Merged
```

After importing the inital state, SourceWarp checks for the existence of user-defined
overrides, i.e., files that should be overwritten or imported into the target
project; SourceWarp copies all files that are contained in the `OVERRIDES_DIR`
(defaults to `/tmp/overrides/`) directory over to the target project. This is
useful in case you want to use a custom GitLab CI configuration.

After applying the overrides, SourceWarp goes through the list of patches to be
applied on the target git repository. After applying them, SourceWarp invokes the
metrics collection a first time. You can easily implement your own metrics
script by configuring them through the `METRIC_SCRIPTS`. Note that
metric scripts have to implement the `MetricScript` interface.

Afterwards, SourceWarp commits and pushes to a branch on the target GitLab project,
creates an MR and auto-accepts it so that it gets merged in. This step can take
a while because SourceWarp has to wait for pipelines to finish in a polling loop. The
polling time of SourceWarp can be adjusted through the variables `POLL_ATTEMPTS`
which specifies the number of attempts to pull updates and
`POLL_INTERVAL_SECONDS` which specifies the seconds that SourceWarp should wait
for updates.

Once the changes are merged into the main branch, SourceWarp runs the metric scripts
again and continues with the next patch unless and until there are no patches
to apply left. Note, that you can also group multiple patches into a single MR
by setting the variable `PATCHES_PER_MR` to the amount of patches you like
to be applied for a single MR. This is useful to control the granularity with
which the history of the source project is replayed, where
`PATCHES_PER_MR=1` is the finest granularity.

If there are no patches to apply left, SourceWarp will write the metrics to the
`OUTPUT` (default `/tmp/stats`) directory. For every metric script, the
output file name is determined by the return value of the `name` method of the
`MetricScript` interface.

### Usage example

A sample configuration of SourceWarp may look as foolows:

``` bash
#!/bin/bash

# credentials
export SRC_GITLAB_TOKEN="..."
export DST_GITLAB_TOKEN="..."

# General parameters
## docker or gitlab
export REPLAY_MODE="docker"
## Path of the record store
export RECORD_STORE="$HOME/sourcewarp/record"
## ignore-list 
export IRRELEVANT="^.gitlab-ci\\.yml$,^.*spec.*$,^.*db.*"
## allow-list file/path names
export RELEVANT_FILE=""
## allow-list pattern 
#export RELEVANT="^.*\\.rb$"
## directory that includes overrides -- files that are overwritten
export OVERRIDES_DIR="/tmp/overrides"

## How often we attempt to aquire metrics
export POLL_ATTEMPTS=10
## How long we wait (s) between poll attemts
export POLL_INTERVAL_SECONDS=10

## Path to the metrics script
export METRIC_SCRIPTS="$PWD/lib/sourcewarp/scripts/parsing.rb"
## How many patches we like to replay at once (a.k.a. patch sampling number)
export PATCHES_PER_MR=1

# Source and Target
## Source project configuration
export SRC_GITLAB_ENDPOINT="https://gitlab.com"
export SRC_DEFAULT_BRANCH_NAME="master"
export SRC_PROJECT="julianthome/bumper"

## Target project configuration -- only active when REPLAY_MODE is set to
# gitlab
export DST_GITLAB_ENDPOINT="$SRC_GITLAB_ENDPOINT"

# for docker mode, we have to use the same branch as the source branch
export DST_DEFAULT_BRANCH_NAME="main"
## local project (mode docker)
export DST_PROJECT="dst_project"
#export DST_PROJECT="julianthome/testtarget"

## Docker settings -- only active when REPLAY_MODE is set to docker
export DOCKER_URL="unix:///var/run/docker.sock"
export DST_DOCKER_OUTFILE="out.json"
# export DST_DOCKER_OUT="/tmp/dockerout"

## Docker settings
export DST_DOCKER_IMAGE="..."
export DST_DOCKER_CMD="..."
```

In `/tmp/overrides` we could have the following file:

```
➜ cat /tmp/overrides/.gitlab-ci.yml
include:
  - template: Dependency-Scanning.gitlab-ci.yml
```

We can execute the record step for `testreplay-source` by invoking the command
below, which will extract all the events (MRs) between 2021-01-21 and
2021-01-26 on the source project.

``` bash
./bin/run_local.sh --record --since "2021-01-21" --until "2021-01-26"
```

In case the command above succeeded, you should be able to see the record store
under `/tmp/record`. Afterwards, we can replay the events on the target project
`testreplay-target` by running the command below.

``` bash
./bin/run_local.sh --replay
```

If the command above succeeded, we can collect the collected metrics from
`/tmp/stats/vulnerability_count.csv`, i.e., the results from the evaluation of
the metric script `vulnerability_count.rb`.

