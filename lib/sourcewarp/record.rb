# frozen_string_literal: true

require 'sourcewarp/events/merge_request'
require 'sourcewarp/config'
require 'sourcewarp/utils'
require 'tmpdir'
require 'time'
require 'fileutils'

module SourceWarp
  class Recorder
    include Logging

    attr_reader :record_store, :skip_diff_lambas

    def initialize(src_client,
                   src_repo,
                   record_store,
                   project_name)
      @src_client = src_client
      @src_repo = src_repo
      @record_store = File.join(record_store, SourceWarp::Utils.slash_to_underscore(project_name))
      @event_classes = [
        Events::MergeRequest
      ]
    end

    def record(conf)
      return if @record_store.empty?

      log.warn("#{@record_store} already exists please move or delete") if Dir.exist?(@record_store)

      log.info("Recording in #{@record_store} using a history logsize of #{conf[:log_size]}...")

      FileUtils.mkdir_p(@record_store)

      @src_repo.setup(conf)

      @event_classes.each do |cls|
        cls.record(@src_client,
                   @src_repo,
                   conf)
      end
    end
  end
end
