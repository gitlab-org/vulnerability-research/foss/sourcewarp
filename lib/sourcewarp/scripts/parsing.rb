#!/usr/bin/env ruby
# frozen_string_literal: true

require 'net/http'
require 'json'

def wait_for_file(name) 
  retries = 0
  while File.exist?(name) == false && retries < 5
    retries += 1
    sleep(3)
  end
  File.exist?(name)
end

def collect(id, _dst_client, dst_project, conf)
  content = ''
  if conf[:replay_mode] == 'gitlab'
    url = "https://gitlab.com/#{dst_project}/-/jobs/artifacts/main/raw/out.json\?job\=testparse"
      loop do
        response = Net::HTTP.get_response(URI.parse(url))
        url = response['location']
        content = response.body
        break unless response.is_a?(Net::HTTPRedirection)
      end
  else
    results = File.join(conf[:record_store],
                        SourceWarp::Utils.slash_to_underscore(conf[:src_project]),
                        SourceWarp::Utils.slash_to_underscore(conf[:dst_project]),
                        'dst_project', 'out.json')

    if id == 0
      return [[0, 0, 0, Time.now]]
    end

    if !wait_for_file(results) 
      abort("file #{results} does not exist")
    end
    puts("fetching #{results}")
    content = File.read(results)
    File.delete(results)
  end

  puts content
  begin
    rdict = JSON.parse(content)
  rescue JSON::ParserError
    abort("malformed JSON content #{content}")
  end

  [[rdict['parsed_files_cnt'], rdict['unparseable_files_cnt'], rdict['time'], Time.now]]
end

def header
  %w[parsed unparseable time timestamp]
end

def name
  'parsing'
end
