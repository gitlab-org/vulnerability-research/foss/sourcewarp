#!/usr/bin/env ruby
# frozen_string_literal: true

def collect(_dst_client, _dst_project, conf)
  sast_report_path = File.join(conf[:record_store],
                               STT::Utils.slash_to_underscore(conf[:src_project]),
                               STT::Utils.slash_to_underscore(conf[:dst_project]),
                               'dst_project', 'gl-sast-report.json')

  cummulative_fingerprints = File.join(conf[:output], 'cummulative-source.json')
  # TODO: Julian expose logger
  # load old fingerprints if present

  old_fingerprints = File.exist?(cummulative_fingerprints) ? JSON.parse(File.read(cummulative_fingerprints)) : []

  unless File.exist?(sast_report_path)
    puts 'no report path'
    return []
  end

  report = JSON.parse(File.read(sast_report_path))

  new_fingerprints = report['vulnerabilities'].map do |vuln|
    "#{vuln['tracking']['positions'].first['fingerprints']['scope+offset']}:#{vuln['cve']}"
  end

  new_fingerprints += old_fingerprints
  new_fingerprints.uniq!

  File.write(cummulative_fingerprints, JSON.pretty_generate(new_fingerprints))

  [[new_fingerprints.size, Time.now]]
end

def header
  %w[count timestamp]
end

def name
  'vulnerability_count'
end
