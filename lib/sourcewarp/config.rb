# frozen_string_literal: true

require 'logger'
require 'optparse'

require_relative 'log'
require_relative 'utils'

module SourceWarp
  # config module for SourceWarp
  module Config
    class Error < StandardError; end

    PARAMAP = {
      default_branch_name: 'DEFAULT_BRANCH_NAME',
      src_gitlab_endpoint: 'SRC_GITLAB_ENDPOINT',
      src_gitlab_token: 'SRC_GITLAB_TOKEN',
      src_project: 'SRC_PROJECT',
      src_project_git_url: 'SRC_PROJECT_GIT_URL',
      record_store: 'RECORD_STORE',
      irrelevant: 'IRRELEVANT',
      relevant: 'RELEVANT',
      relevant_file: 'RELEVANT_FILE',
      dst_gitlab_endpoint: 'DST_GITLAB_ENDPOINT',
      dst_gitlab_token: 'DST_GITLAB_TOKEN',
      dst_project: 'DST_PROJECT',
      dst_project_git_url: 'DST_PROJECT_GIT_URL',
      overrides: 'OVERRIDES_DIR',
      poll_interval_seconds: 'POLL_INTERVAL_SECONDS',
      poll_attempts: 'POLL_ATTEMPTS',
      metric_scripts: 'METRIC_SCRIPTS',
      events_until: 'EVENTS_UNTIL',
      events_since: 'EVENTS_SINCE',
      patches_per_mr: 'PATCHES_PER_MR',
      log_size: 'LOG_HISTORY_SIZE',
      output: 'OUTPUT',
      replay_mode: 'REPLAY_MODE',
      docker_image: 'DST_DOCKER_IMAGE',
      docker_outfile: 'DST_DOCKER_OUTFILE',
      docker_out: 'DST_DOCKER_OUT',
      docker_url: 'DOCKER_URL',
      docker_cmd: 'DST_DOCKER_CMD'
    }.freeze

    def self.comma_sepd(val)
      val.split(',').map(&:strip).to_a
    end

    # rubocop:disable Style/MutableConstant
    CONFIG = {
      src_default_branch_name: Utils.default_env('SRC_DEFAULT_BRANCH_NAME', 'main'),
      dst_default_branch_name: Utils.default_env('DST_DEFAULT_BRANCH_NAME', 'main'),
      src_gitlab_endpoint: Utils.default_env('SRC_GITLAB_ENDPOINT', 'https://gitlab.com'),
      src_gitlab_token: Utils.default_env('SRC_GITLAB_TOKEN', nil),
      src_project: Utils.default_env('SRC_PROJECT', nil),
      record_store: Utils.default_env('RECORD_STORE', ''),
      irrelevant: comma_sepd(Utils.default_env('IRRELEVANT', '')),
      relevant: comma_sepd(Utils.default_env('RELEVANT', '')),
      relevant_file: Utils.default_env('RELEVANT_FILE', ''),
      dst_gitlab_endpoint: Utils.default_env('DST_GITLAB_ENDPOINT', nil),
      dst_gitlab_token: Utils.default_env('DST_GITLAB_TOKEN', nil),
      dst_project: Utils.default_env('DST_PROJECT', 'dst_project'),
      dst_project_git_url: Utils.default_env('DST_PROJECT_GIT_URL', nil),
      overrides: Utils.default_env('OVERRIDES_DIR', ''),
      poll_interval_seconds: Utils.default_env('POLL_INTERVAL_SECONDS', 10),
      poll_attempts: Utils.default_env('POLL_ATTEMPTS', 10),
      metric_scripts: comma_sepd(Utils.default_env('METRIC_SCRIPTS', '')),
      events_until: Utils.default_env('EVENTS_UNTIL', Time.now),
      events_since: Utils.default_env('EVENTS_SINCE', Time.at(0)),
      patches_per_mr: Utils.default_env('PATCHES_PER_MR', 5),
      log_size: Utils.default_env('LOG_HISTORY_SIZE', 1_000_000),
      output: Utils.default_env('OUTPUT', 'sourcewarp-metrics.csv'),

      replay_mode: Utils.default_env('REPLAY_MODE', 'docker'),
      docker_image: Utils.default_env('DST_DOCKER_IMAGE', ''),
      docker_outfile: Utils.default_env('DST_DOCKER_OUTFILE', ''),
      docker_out: Utils.default_env('DST_DOCKER_OUT', ''),
      docker_url: Utils.default_env('DOCKER_URL', 'unix:///var/run/docker.sock'),
      docker_cmd: Utils.default_env('DST_DOCKER_CMD', '/analyzer run')
    }
    # rubocop:enable Style/MutableConstant

    # print settings that are safe to print
    def self.show_config_safe
      safe_keys = %i[
        gitlab_endpoint
        labels
        projects
        groups
        assignee
        author
      ]
      puts 'Config:'
      safe_keys.each do |key|
        puts format('  %<key>-25s %<value>s', key: key, value: CONFIG[key])
      end
    end

    def self.parse_args
      OptionParser.new do |opts|
        opts.banner = <<~BANNER

           SourceWarp

                     ╱╱╲ ◀━━━━━───┄┄
                    ╱╱∴∴╲
                   ╱╱∴∷∴∴╲ ◀━───┄
                  ╱╱∴   ∵∴╲
                 ╱╱       ∴╲
                  ─────────────────────
                             ╲∵       ╱╱
                              ╲∵∴   :╱╱
                        ┄───━▶ ╲∵∷∷∵╱╱
                                ╲∵∵╱╱
                     ┄┄───━━━━━▶ ╲╱╱

          Usage: sourcewarp [options]
        BANNER
        opts.on('--src_default-branch-name DEFAULT_BRANCH_NAME', 'Default source branch name (SRC_DEFAULT_BRANCH_NAME)')
        opts.on('--dst_default-branch-name DEFAULT_BRANCH_NAME', 'Default dst branch name (DST_DEFAULT_BRANCH_NAME)')
        opts.on('--src-gitlab-endpoint ENDPOINT', 'Source GitLab endpoint (SRC_GITLAB_ENDPOINT)')
        opts.on('--src-gitlab-token TOKEN', 'Source GitLab token (SRC_GITLAB_TOKEN)')
        opts.on('--src-project PROJECT', 'Source GitLab project path (SRC_PROJECT)')
        opts.on('--src-project-git-url GIT_URL', 'URL to use when cloning/pushing to this repository (SRC_PROJECT_GIT_URL)')
        opts.on('--record-store RECORD_STORE', 'Record store (RECORD_STORE)')
        opts.on('--irrelevant IRRELEVANT',
                'file patterns to ignore used for initial import and git history analysis (IRRELEVANT)') do |irrelevant|
                  CONFIG[:irrelevant] = comma_sepd(irrelevant)
                end
        opts.on('--relevant RELEVANT',
                'file patterns to be included used for git history analysis (RELEVANT)') do |relevant|
                  CONFIG[:relevant] = comma_sepd(relevant)
                end
        opts.on('--relevant-file RELEVANT_FILE',
                'Path to the file that contains the allow-list (RELEVANT_FILE)') do |relevant_file|
          if File.exist?(relevant_file)
            log.info("parsing include file list from #{relevant_file}")
            CONFIG[:relevant] += File.readlines(relevant_file).map do |line|
              ".*/#{Regexp.escape(line.chomp)}"
            end
          end
        end
        opts.on('--dst-gitlab-endpoint ENDPOINT', 'Source GitLab endpoint (DST_GITLAB_ENDPOINT)')
        opts.on('--dst-gitlab-token TOKEN', 'Source GitLab token (DST_GITLAB_TOKEN)')
        opts.on('--dst-project PROJECT', 'Source GitLab project path (DST_PROJECT)')
        opts.on('--dst-project-git-url GIT_URL', 'URL to use when cloning/pushing to this repository (DST_PROJECT_GIT_URL)')

        opts.on('--overrides OVERRIDES_DIR', 'Source GitLab project path (OVERRIDES_DIR)')

        opts.on('--poll-interval-seconds POLL_INTERVAL',
                'Max seconds to wait for MRs to be merged in (POLL_INTERVAL_SECONDS)') do |_poll_interval|
                  CONFIG[:poll_interval] = poll_attempts.to_i
                end

        opts.on('--poll-attempts POLL_ATTEMPTS', 'Max attempts for MR to gets merged in (POLL_ATTEMPTS)') do |poll_attempts|
          CONFIG[:poll_attempts] = poll_attempts.to_i
        end
        opts.on('--metric-scripts SCRIPTS',
                'Comma-separated paths of user-defined metric scripts (METRIC_SCRIPTS)') do |scripts|
                  CONFIG[:metric_scripts] = comma_sepd(scripts)
                end
        opts.on('--output', 'The output metrics file (OUTPUT)')
        opts.on('--replay-mode', 'Replaying by running a docker image (docker) or by creating MRs (gitlab) (REPLAY_MODE)')
        opts.on('--docker-image', 'Docker image to be used (only with replay-mode: docker (DST_DOCKER_IMAGE)')
        opts.on('--docker-outfile', 'Name of the output file produced by the docker container (+timestamp is stored)')
        opts.on('--docker-out', 'Directory where the output files of the docker container are stored')
        opts.on('--docker-url', 'Docker URL to be used')
        opts.on('--docker-cmd', 'Docker command to be used')

        opts.on('--until EVENTS_UNTIL', 'MRs younger than (EVENTS_UNTIL)') do |event|
          CONFIG[:events_until] = Time.parse(event)
        end
        opts.on('--since EVENTS_SINCE', 'MRs older than (EVENTS_SINCE)') do |event|
          CONFIG[:events_since] = Time.parse(event)
        end
        opts.on('--log-size LOG_HISTORY_SIZE',
                'Max log size of the git history to consider (LOG_HISTORY_SIZE)') do |log_size|
                  CONFIG[:log_size] = log_size.to_i
                end
        opts.on('--patches-per-mr PATCHES_PER_MR',
                'number of patches applied per MR (PATCHES_PER_MR)') do |patches_per_mr|
                  CONFIG[:patches_per_mr] = patches_per_mr.to_i
                end
        opts.on('-d', '--debug', 'Run in debug mode') do |_d|
          Logging.log_level = Logger::DEBUG
        end
        opts.on('-h', '--help', 'Print help message') do
          puts opts
          exit 1
        end

        CONFIG[:record] = false
        opts.on('--record', 'Record MRs on repository') do |_d|
          CONFIG[:record] = true
        end

        CONFIG[:replay] = false
        opts.on('--replay', 'Replay MRs on repository') do |_d|
          CONFIG[:replay] = true
        end
      end.parse!(into: CONFIG)

      # command-line arguments are stored as CONFIG['email-from'.to_sym] instead
      # of CONFIG[:email_from]. This transforms all parsed cmd-line args into the
      # underscore version
      CONFIG.each do |k, v|
        next unless k.to_s =~ /\w*-\w*/

        CONFIG[k.to_s.gsub('-', '_').to_sym] = v
      end

      errors = []
      errors << 'record or replay flag must be set' unless CONFIG[:replay] || CONFIG[:record]
      errors << if CONFIG[:record]
                  %i[src_gitlab_endpoint
                     src_gitlab_token
                     src_project].map do |s|
                    "#{PARAMAP[s]} must be set" if CONFIG[s].to_s.empty?
                  end
                else
                  case CONFIG[:replay_mode]
                  when 'gitlab'
                    %i[dst_gitlab_endpoint
                       dst_gitlab_token
                       dst_project].map do |s|
                      "#{PARAMAP[s]} must be set" if CONFIG[s].to_s.empty?
                    end
                  when 'docker'
                    %i[dst_project
                       docker_image
                       docker_outfile
                       docker_url
                       docker_cmd].map do |s|
                      "#{PARAMAP[s]} must be set" if CONFIG[s].to_s.empty?
                    end
                  end
                end
      errors.flatten!
      errors.compact!

      unless errors.count.zero?
        raise Error, "Configuration Error:\n\n#{errors.join("\n")}"
      end

      CONFIG[:src_gitlab_endpoint] = CONFIG[:src_gitlab_endpoint].delete_suffix('/')
      CONFIG[:dst_gitlab_endpoint] = CONFIG[:dst_gitlab_endpoint].delete_suffix('/')
      if File.exist?(CONFIG[:relevant_file])
        CONFIG[:relevant] += File.readlines(CONFIG[:relevant_file]).map do |line|
          ".*/#{Regexp.escape(line.chomp)}"
        end
      end

      if CONFIG[:replay_mode] == 'docker'
      elsif CONFIG[:replay_mode] != 'gitlab'
        raise Error, "Replay mode must be either 'docker' or 'gitlab'"
      end

      CONFIG
    end
  end
end
