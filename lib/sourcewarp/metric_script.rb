# frozen_string_literal: true

require_relative 'log'
require 'cgi'

module SourceWarp
  class MetricScript
    def initialize; end

    def collect(_dst_client, _dst_project, _conf)
      raise NotImplementedError('collect must be implemented')
    end

    def name
      raise NotImplementedError('collect must be implemented')
    end

    def header
      raise NotImplementedError('collect must be implemented')
    end
  end

  module MetricScriptMod
    def self.create_mod(path)
      abort("#{path} does not exist") unless File.exist?(path)
      Class.new do
        class_eval File.read(path)
      end.new
    end
  end
end
