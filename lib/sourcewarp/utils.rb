# frozen_string_literal: true

require 'fileutils'
require 'sourcewarp/log'
require 'find'

# Utility module
module SourceWarp
  module Utils
    class << self
      include Logging

      def default_env(key, default)
        val = ENV[key]
        # gitlab-ci quirk about variables - variables must be explicitly copied into
        # the variables for a new job. if the variable is undefined, the value of
        # the copied (empty) variable is '$VARIABLE'
        return default if ["$#{key}", nil, ''].include?(val)

        val
      end

      def slash_to_underscore(str)
        str.gsub('/', '_')
      end

      def copy_recursively(source_dir, target_dir,
                           exclude_patterns = [])
        abort("#{source_dir} does not exist") unless Dir.exist?(source_dir)

        FileUtils.mkdir(target_dir) unless File.exist?(target_dir)

        files2copy = []
        Find.find(source_dir) do |f|
          next if exclude_patterns.any? { |e| f.match?(e) }

          files2copy << f
        end

        log.info("#{files2copy.size} files to copy")
        files2copy.each do |f|
          target = f.sub(/^#{source_dir}/, target_dir)
          if File.directory?(f)
            FileUtils.mkdir(target) unless File.exist?(target)
          else
            FileUtils.copy(f, target)
          end
        end
      end

      def rewrite_diff(difffile,
                       file_patterns_to_exclude,
                       file_patterns_to_include = [])
        s = StringIO.new
        exclude = false

        File.open(difffile, 'r') do |file_handle|
          file_handle.each_line do |line|
            matches = line.match('diff.+ ([^ ]+) ([^ ]+)')
            unless matches.nil? || matches.captures.empty?
              exclude = [matches[1], matches[2]].any? do |f|
                file_patterns_to_exclude.any? { |e| f.match?(e) }
              end
              if file_patterns_to_include.any?
                exclude |= [matches[1], matches[2]].any? do |f|
                  file_patterns_to_include.none? { |e| f.match?(e) }
                end
              end
            end

            next if exclude

            s << line
          end
        end
        s.string
      end
    end
    # self
  end
  # Utils
end
# SourceWarp
