# frozen_string_literal: true

require 'git'
require 'timeout'
require 'sourcewarp/log'
require 'fileutils'
require 'sourcewarp/exceptions/apply_patch_and_push_exception'
require 'open3'

module SourceWarp
  class Gittracker
    include Logging

    attr_reader :project_path, :target_dir_git, :target_dir_pfx, :target_dir_init, :target_dir_diffs, :repo, :default_branch_name, :is_dest

    def initialize(project_path, record_store, repo_name: 'git', project_git_url: nil, default_branch_name: 'main', is_dest: false)
      @project_path = project_path
      @full_url = project_git_url || "git@gitlab.com:#{@project_path}"
      @git_repo_name = repo_name
      @init_state_dir_name = 'src_project_init_files'

      @target_dir_pfx = File.join(record_store, SourceWarp::Utils.slash_to_underscore(project_path))
      @target_dir_git = File.join(@target_dir_pfx, @git_repo_name)
      @target_dir_init = File.join(@target_dir_pfx, @init_state_dir_name)
      @target_dir_diffs = File.join(@target_dir_pfx, 'diffs')
      @start_sha = ''
      @default_branch_name = default_branch_name
      @is_dest = is_dest
    end

    def setup(conf)
      # setup a local git repository
      if conf[:replay_mode] == 'docker' && @is_dest 
        if !Dir.exist?(@target_dir_git) 
          FileUtils.mkdir_p(@target_dir_git)
          FileUtils.chmod(0o0700, @target_dir_git)
          @repo = Git.init(@target_dir_git)
          File.open(File.join(@target_dir_git, 'README.md'), 'w+') do |file|
            file.write('## init')
          end
          @repo.add('README.md')
          @repo.commit('initial commit')
        end
      elsif Dir.exist?(@target_dir_git)
        log.info("#{@project_path} already present in #{@target_dir_git}")
        @repo = Git.open(@target_dir_git)
        gracefully_pull_updates
      else
        log.info("cloning #{@project_path} to #{@target_dir_git}")
        @repo = Git.clone(@full_url, @git_repo_name, path: @target_dir_pfx)
        @repo.branch(@default_branch_name).checkout
      end

      @repo.config('user.email', 'gitlab-bot@gitlab.com')
      @repo.config('user.name', 'GitLab Bot')
      @repo.config("lfs.#{@full_url}/info/lfs.locksverify", true)
      @repo.config('lfs.allowincompletepush', true)

      update_start_sha
    end

    def update_start_sha
      @start_sha = @repo.log.first.sha
    end

    def gracefully_pull_updates
      @repo.pull
    rescue Git::GitExecuteError => e
      log.warn(e.message)
    end

    # extract the files that correspond to the first known (file-) state of
    # a git repository
    def extract_initial_state(sorted_shas, irrelevant)
      # events list is sorted -- get the oldest one first
      init_sha = sorted_shas.first
      abort('No initial sha detected or configured') if init_sha.empty?

      log.info("initial event sha is #{init_sha}")

      log.info("checking out #{init_sha}")
      # checkout initial state
      @repo.checkout(init_sha)

      log.info("copy #{@target_dir_git} to #{@target_dir_init}")
      # copy over the initial state
      Utils.copy_recursively(@target_dir_git, @target_dir_init, ['\\.git'] + irrelevant)
    end

    # extract a sequence of diffs from the meta-information attached to the
    # MR
    def serialize_diff_sequence(sorted_shas,
                                irrelevant,
                                relevant)
      sha_ptr = sorted_shas.first
      log.info("Serializing diff sequence with filters: irrelevant (#{irrelevant}), relevant: (#{relevant})")

      idx = 0
      FileUtils.mkdir_p(@target_dir_diffs)
      sorted_shas.drop(1).each do |mr_sha|
        diffs = @repo.diff(sha_ptr, mr_sha).path('.').patch
        difffile = File.join(@target_dir_diffs, "#{idx}.patch")

        File.open(difffile, 'w') { |f| f.puts(diffs) }
        rewritten_diff = SourceWarp::Utils.rewrite_diff(difffile, irrelevant, relevant)

        if rewritten_diff.strip.empty?
          File.delete(difffile)
          next
        end

        log.info("writing #{difffile} for #{sha_ptr}:#{mr_sha}")
        File.open(difffile, 'w') { |f| f.puts(rewritten_diff) }

        sha_ptr = mr_sha
        idx += 1
      end
    end

    def wait_for_updates(poll_attempts, poll_interval_seconds)
      log.info("waiting for updates -- max attempts: #{poll_attempts}")
      log.info("checking out #{@default_branch_name}")
      @repo.checkout(@default_branch_name)
      sha_before = @repo.log.first.sha

      (0..poll_attempts.to_i).each do
        @repo.pull
        sha_after = @repo.log.first.sha
        return true if sha_after != sha_before

        log.info("sleep #{poll_interval_seconds} seconds ...")

        sleep(poll_interval_seconds.to_i)
      end

      false
    end

    # extract a sequence of diffs from the meta-information attached to the
    # MR
    def apply_patch_and_push(patch, branchname)
      log.info("apply patch #{patch} on branch: #{branchname}")
      @repo.branch(branchname).checkout

      # gracefully ignore issues that could arise when applying patches
      @repo.chdir { 
        stdout_str, stderr_str, status = Open3.capture3("git apply --reject #{patch}")
        log.info("output: #{stdout_str}")
        log.info("errors: #{stderr_str}")
        log.info("status: #{status}")
      }
      begin
        @repo.add(all: true)
        @repo.commit("applied #{patch}")
        @repo.push('origin', @repo.current_branch, f: true)
      rescue Git::GitExecuteError => e
        abort(e.message)
        raise ApplyPatchAndPushException, e.message
      end

      @repo.current_branch
    end

    # extract a sequence of diffs from the meta-information attached to the
    # MR
    def apply_patch_and_commit(patch)
      branchname = @repo.current_branch
      log.info("apply patch #{patch} on branch: #{branchname}")
        @repo.reset
      @repo.checkout(branchname)

      # gracefully ignore issues that could arise when applying patches

      @repo.chdir { 
        stdout_str, stderr_str, status = Open3.capture3("git apply --reject #{patch}")
        log.info("output: #{stdout_str}")
          log.info("errors: #{stderr_str}")
          log.info("status: #{status}")
      }
      begin
        @repo.add(all: true)
      rescue Git::GitExecuteError => e
        log.warn(e.message)
        abort(e.message)
      end
      begin
        @repo.commit("applied #{patch}")
      rescue Git::GitExecuteError => e
        abort(e.message)
        raise ApplyPatchAndPushException, e.message
      end

      @repo.current_branch
      end

    def addall(msg)
      @repo.add(all: true)
      begin
        @repo.commit(msg)
      rescue Git::GitExecuteError => e
        log.info(e.message)
      end
    end

    # extract a sequence of diffs from the meta-information attached to the
    # MR
    def import_initial_state(src_dir)
      log.info("importing #{src_dir} to #{@target_dir_git}")
      gracefully_pull_updates

      # copy over the initial state
      Utils.copy_recursively(src_dir, @target_dir_git)
      return if @repo.diff('HEAD').size.zero?

      @repo.add(all: true)
      @repo.commit('import initial state')
    end

    def push
      branch = @repo.current_branch

      if branch.nil?
        @repo.checkout(@default_branch_name)
        branch = @repo.current_branch
      end
      begin
        @repo.push('origin', branch, f: true)
      rescue Git::GitExecuteError => e
        log.warn(e.message)
        abort(e.message)
      end

      update_start_sha
    end

    # extracts all commit that happened since since_date until
    # until_date and returns a list of strings (commit SHAs)
    def get_all_commit_merges_between(since_date, until_date, logsize = 1_000_000)
      log.info("get MR commits between #{since_date.strftime('%F')} and #{until_date.strftime('%F')}")
      @repo.log(logsize).since(since_date.iso8601).until(until_date.iso8601)
           .reject { |c| c.parents.size < 2 }.map(&:sha)
    end
  end
end
