# frozen_string_literal: true

require 'sourcewarp/log'
require 'sourcewarp/events/base'
require 'open3'

module SourceWarp
  module Events
    class Docker 
      attr_reader :image, :url, :bind, :cmd

      def initialize(image, url, bind, cmd)
        @image = image
        @url = url
        @bind = bind
        # be aware of 
        abort("no shell escapes") if cmd.include?("'")
        @cmd = "docker run --platform linux/amd64 -v #{@bind}:/code #{image} /bin/sh -c '#{cmd}'"
      end

      def run 
        sleep(2)
        stdout_str, stderr_str, status = Open3.capture3(@cmd)
        puts "output: #{stdout_str}"
        puts "errors: #{stderr_str}"
        puts "status: #{status}"
        sleep(2)
      end

      def info 
        """docker-image: #{self.image}
docker-url: #{@url}
docker-bind: #{@bind}:/code
docker-cmd: #{@cmd}
"""
      end
    end

    class MergeRequest < EventBase
      MR_LABELS = [].freeze

      # Return a list of events between start_date and end_date
      def self.record(_src_client,
                      src_repo,
                      conf)
        sorted_shas = src_repo.get_all_commit_merges_between(conf[:events_since],
                                                             conf[:events_until],
                                                             conf[:log_size].to_i)
        # start with the oldest sha

        sorted_shas.reverse!

        abort('No shas etracted') if sorted_shas.empty?

        # translate and write a diff sequence that corresonds to the
        # list of MRs
        src_repo.extract_initial_state(sorted_shas, conf[:irrelevant])
        src_repo.serialize_diff_sequence(sorted_shas,
                                         conf[:irrelevant],
                                         conf[:relevant])
      end

      attr_reader :mr_data, :mr_diff, :ignore_diff_lambdas

      # Replays this event into the destination project, calling
      # metrics_collector.collect at each available metric collection state.
      # The metric collection states are defined by the event (i.e. are
      # event-specific).
      def self.replay(src_repo,
                      dst_repo,
                      dst_client,
                      metrics_collector,
                      conf)

        dst_project = conf[:dst_project]
        log = Logger.new($stdout)
        log.info('Importing initial state')

        FileUtils.chmod_R(0777, src_repo.target_dir_init)
        dst_repo.import_initial_state(src_repo.target_dir_init)

        log.info('Importing overrides if any')
        Utils.copy_recursively(conf[:overrides], dst_repo.target_dir_git)
        dst_repo.addall('overrides')

        dst_repo.push unless conf[:replay_mode] == 'docker'

        log.info('Replaying ...')

        patches = []
        Dir.chdir(src_repo.target_dir_diffs) do |_indir|
          Dir.glob('*.patch')
             .map { |f| File.basename(f, '.patch').to_i }
             .sort.map { |f| "#{f}.patch" }.each do |patch|
            patches << File.join(src_repo.target_dir_diffs, patch)
          end
        end

        mridx = 0
        log.info("#{patches.size} patches to apply")
        patches.each_with_index do |patchabs, idx|
          branchlabel = "mr_#{mridx}"

          begin
            dst_repo.apply_patch_and_push(patchabs, branchlabel) if conf[:replay_mode] == 'gitlab'
            dst_repo.apply_patch_and_commit(patchabs) if conf[:replay_mode] == 'docker'
          rescue ApplyPatchAndPushException => e
            log.warn("#{e.message} ... skipping")
            next
          end

          rest = idx % conf[:patches_per_mr].to_i
          unless rest.zero? || idx == (patches.size - 1)
            log.info('next patch')
            next
          end

          if conf[:replay_mode] == 'docker'
            log.info('starting docker')

            srcfs = File.join(conf[:record_store],
                              SourceWarp::Utils.slash_to_underscore(conf[:src_project]),
                              SourceWarp::Utils.slash_to_underscore(conf[:dst_project]),
                              'dst_project')
            container = Docker.new(conf[:docker_image],conf[:docker_url],srcfs,conf[:docker_cmd])
            log.info(container.info)
            container.run
          else
            # replay by means of MR
            # TODO: outsource into separate class later
            log.info("preparing MR on #{dst_project} [#{conf[:dst_default_branch_name]}]")
            mr_payload = {
              source_branch: branchlabel,
              description: branchlabel,
              target_branch: conf[:dst_default_branch_name],
              remove_source_branch: true,
              labels: MR_LABELS
            }
            mr = dst_client.create_merge_request(dst_project, branchlabel, mr_payload)
            sleep(3)
            log.info("auto accepting #{mr.iid}")
            begin
              accept_payload = {
                merge_when_pipeline_succeeds: true
              }
              # can fail if no pipeline is running
              dst_client.accept_merge_request(dst_project, mr.iid, accept_payload)
            rescue StandardError
              dst_client.accept_merge_request(dst_project, mr.iid)
            end

            sleep(1)
            ok = dst_repo.wait_for_updates(conf[:poll_attempts], conf[:poll_interval_seconds])

            unless ok
              log.error("Could not apply #{patchabs}")
              exit(1)
            end

          end

          log.info('Collecting stats ...')
          metrics_collector.collect(mridx)
          mridx += 1
        end
      end
    end
  end
end
