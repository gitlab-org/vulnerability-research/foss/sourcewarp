# frozen_string_literal: true

require 'sourcewarp/log'

module SourceWarp
  module Events
    class EventBase
      include Logging

      # Return a list of events between start_date and end_date
      def self.record(_src_client,
                      _src_repo,
                      _project,
                      _start_date: nil,
                      _end_date: nil)
        raise NotImplementedError('collect must be implemented')
      end

      # Replays this event into the destination project, calling
      # metrics_collector.collect at each available metric collection state.
      # The metric collection states are defined by the event (i.e. are
      # event-specific).
      def replay(_dst_client,
                 _dst_repo,
                 _project,
                 _metrics_collector,
                 _poll_attemts,
                 _poll_interval)
        raise NotImplementedError('collect must be implemented')
      end
    end
  end
end
