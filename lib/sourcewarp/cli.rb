# frozen_string_literal: true

require 'gitlab'

require 'sourcewarp/gittracker'
require 'sourcewarp/config'
require 'sourcewarp/log'
require 'sourcewarp/record'
require 'sourcewarp/replay'
require 'sourcewarp/metrics_collector'

module SourceWarp
  module CLI
    def self.run
      begin
        SourceWarp::Config.parse_args
      rescue SourceWarp::Config::Error => e
        puts e.to_s
        exit 1
      end
      SourceWarp::Config.show_config_safe
      conf = SourceWarp::Config::CONFIG

      src_client = Gitlab.client(
        endpoint: "#{conf[:src_gitlab_endpoint]}/api/v4",
        private_token: conf[:src_gitlab_token]
      )
      src_repo = Gittracker.new(
        conf[:src_project],
        conf[:record_store],
        repo_name: 'src_project',
        default_branch_name: conf[:src_default_branch_name],
        is_dest: false
      )

      if conf[:record]
        # Record
        recorder = Recorder.new(src_client,
                                src_repo,
                                conf[:record_store],
                                conf[:src_project])

        recorder.record(conf)
      else
        # Replay
        dst_client = Gitlab.client(
          endpoint: "#{conf[:dst_gitlab_endpoint]}/api/v4",
          private_token: conf[:dst_gitlab_token]
        )
        dst_repo = Gittracker.new(
          conf[:dst_project],
          src_repo.target_dir_pfx,
          repo_name: 'dst_project',
          project_git_url: conf[:dst_project_git_url],
          default_branch_name: conf[:dst_default_branch_name],
          is_dest: true
        )

        metrics_collector = MetricsCollector.create(dst_client,
                                                    conf[:dst_project],
                                                    conf[:metric_scripts],
                                                    conf)

        replayer = Replayer.new(
          src_repo,
          dst_client,
          dst_repo,
          metrics_collector
        )
        replayer.replay(conf)
        metrics_collector.save(conf[:output])
      end
    end
  end
end
