# frozen_string_literal: true

require 'sourcewarp/log'
require 'sourcewarp/exceptions/apply_patch_and_push_exception'

module SourceWarp
  class Replayer
    include Logging

    def initialize(src_repo,
                   dst_client,
                   dst_repo,
                   metrics_collector)

      @src_repo = src_repo
      @dst_client = dst_client
      @dst_repo = dst_repo
      @metrics_collector = metrics_collector

      @event_classes = [
        Events::MergeRequest
      ]
    end

    def replay(conf)
      unless Dir.exist?(conf[:record_store])
        log.error("#{conf[:record_store]} does not exist -- unable to replay events")
        exit(1)
      end

      @src_repo.setup(conf)

      if Dir.exist?(@dst_repo.target_dir_git)
        FileUtils.mv(@dst_repo.target_dir_git, "#{@dst_repo.target_dir_git}_#{Time.now}")
      end

      @dst_repo.setup(conf)

      log.info('Replaying...')

      @event_classes.each do |cls|
        cls.replay(
          @src_repo,
          @dst_repo,
          @dst_client,
          @metrics_collector,
          conf
        )
      end
    end
  end
end
