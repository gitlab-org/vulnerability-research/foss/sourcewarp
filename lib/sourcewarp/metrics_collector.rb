# frozen_string_literal: true

require 'sourcewarp/log'
require 'sourcewarp/metric_script'

module SourceWarp
  class MetricsCollector
    include Logging

    attr_accessor :collection_scripts

    def self.create(dst_client,
                    dst_project,
                    script_paths,
                    conf)

      handle = new(dst_client, dst_project, script_paths, conf)
      handle.collection_scripts = script_paths.map do |path|
        MetricScriptMod.create_mod(path)
      end
      handle
    end

    def initialize(dst_client, dst_project, script_paths, conf)
      @collection_scripts = []
      @dst_client = dst_client
      @script_path = script_paths
      @dst_project = dst_project
      @script_paths = script_paths
      @conf = conf
      @datamap = {}
    end

    def collect(id)
      log.info('collect')
      @collection_scripts.each do |script|
        log.info("computing stats with #{script.name}")

        # TODO: Julian create a copy of conf ... we do not want the plugins
        # writing tot this object
        data = script.collect(id, @dst_client, @dst_project, @conf)
        @datamap[script.name] ||= []
        @datamap[script.name] << data

        log.info("data points for #{script.name}: #{@datamap[script.name].size}")
      end
    end

    def save(output)
      unless Dir.exist?(output)
        FileUtils.mkdir_p(output)
      end

      log.info('save')
      @collection_scripts.each do |script|
        target_csv = File.join(output, File.join("#{script.name}.csv"))
        log.info("#{script.name}: writing results to #{target_csv}")

        @datamap[script.name] = [] unless @datamap.key?(script.name)

        CSV.open(target_csv, 'wb') do |csv|
          csv << script.header
          @datamap[script.name].each do |dataset|
            dataset.each do |row|
              csv << row.flatten
            end
          end
        end
      end
    end
  end
end
