FROM ruby:3-alpine AS builder

COPY . /sourcewarp
WORKDIR /sourcewarp

RUN apk update && apk --no-cache add git make gcc libc-dev && gem install bundler && bundle config set --local frozen 'true' && bundle install

CMD ["/sourcewarp/exe/sourcewarp" ]
